# Commands Robot Framework

#robot --randomize ALL -i demoRdemo2 --loglevel INFO Tests
#robot --randomize ALL -i demoRdemo2 --loglevel TRACE Tests
#robot --randomize ALL -i demoRdemo2 --loglevel INFO DEBUG Tests
#robot --randomize ALL -I demo2 Tests
#robot --randomize ALL -I demo2ORdemo2 Tests

#robot -d Output --loglevel TRACE Tests\Test1.robot


#Mobile Commands
#robot -d Output --loglevel TRACE Tests\Open_Application.robot
#robot -d Output --loglevel Tests\Open_Application.robot
#robot -d Output --loglevel TRACE TestMobile\TestLogin.robot

#Validar el emulador de android
#CMD : emulator -list-avds
#Inicar el emulador Android desde el cmd :
#cd C:\Users\GabrielPC\AppData\Local\Android\Sdk\emulator
#emulator -avd Pixel_3a_API_28 (Nombredeldispositivo Ejemplo)
#Levantar el lector de styles MOBILE
#uiautomatorviewer &

#Formas de setear los xpath,class,id en Mobile.
#chat21.android.demo:id/login
#//android.widget.Button[@text="Login"]
#//android.widget.Button[contains(@text,"Login")]
#//android.widget.Button[contains(@resource-id,"Login")]
#//android.widget.Button[@resource-id='chat21.android.demo:id/login' and @text='Login']
#//android.widget.LinearLayout//android.widget.Button[@text,"Login"]


#version in cmd
#adb shell getprop ro.build.version.release

#Capabilities Commands to Run Parallel Executions
#First Device
#appium --default-capabilities {\"adbPort\":5037,\"systemPort\":8201,\"newCommandTimeout\":0,\"automationName\":\"UiAutomator2\",\"udid\":\"emulator-5554\"}
#Second Device
#appium --port 4725 --default-capabilities {\"adbPort\":5038,\"systemPort\":8202,\"newCommandTimeout\":0,\"automationName\":\"UiAutomator2\",\"udid\":\"emulator-5556\"}