*** Settings ***
Library     AppiumLibrary

*** Variables ***
${LOGIN-SUBMIT-BUTTON01}        chat21.android.demo:id/login
${LOGIN-SUBMIT-BUTTON02}        //android.widget.Button[@text="Login"]
${LOGIN-SUBMIT-BUTTON03}        //android.widget.Button[contains(@text,"Login")]
${LOGIN-SUBMIT-BUTTON04}        //android.widget.Button[contains(@resource-id,'login')]
${LOGIN-SUBMIT-BUTTON05}        //android.widget.Button[@resource-id='chat21.android.demo:id/login' and @text='Login']
${LOGIN-SUBMIT-BUTTON06}        //android.widget.LinearLayout//android.widget.Button[@text="Login"]

*** Test Cases ***
Open_Application
        Open Application    http://localhost:4723/wd/hub      platformName=Android     deviceName=emulator-5554     appPackage=chat21.android.demo       appActivity=chat21.android.demo.SplashActivity        automationName=Uiautomator2
        Wait Until Page Contains Element       ${LOGIN-SUBMIT-BUTTON01}
        Wait Until Page Contains Element       ${LOGIN-SUBMIT-BUTTON02}
        Wait Until Page Contains Element       ${LOGIN-SUBMIT-BUTTON03}
        Wait Until Page Contains Element       ${LOGIN-SUBMIT-BUTTON04}
        Wait Until Page Contains Element       ${LOGIN-SUBMIT-BUTTON05}
        Wait Until Page Contains Element       ${LOGIN-SUBMIT-BUTTON06}