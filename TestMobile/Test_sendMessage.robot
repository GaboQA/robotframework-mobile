*** Settings ***
Library     AppiumLibrary
Resource    ../Resources/android-res.robot
Library    BuiltIn
Library    SeleniumLibrary


*** Test Cases ***
TestLogin
        Open Chat21 Application
        Wait Until Page Contains Element        ${LOGIN-PAGE-FIELD}
        Signing With User       ${USER1-DETAILS}[email]    ${USER1-DETAILS}[password]
        Go to Chat      ${CONTACT-01}
        Send Message Randoms        ${MES-DEF-CLS1}
