*** Settings ***
Library     AppiumLibrary
Resource    password.robot
Library    BuiltIn
Library    SeleniumLibrary

*** Variables ***

${APPIUM-PORT-DEVICE1}      4723
#${APPIUM-PORT-DEVICE2}      4723

#*** Android Continue Button ***
${ANDROID10-CONTINUE-BUTTON}        id=com.android.permissioncontroller::id/continue_button
${ANDROID10-OK-BUTTON}              //android.widget.Button[@text="OK"]

#*** Test Variables ***
&{USER1-DETAILS}        email=gabrielqqquesada@gmail.com        password=${PASSWORD}
&{USER2-DETAILS}        email=gabrielqqquesada@gmail.com        password=${PASSWORD}

#*** Login Page ***
${delay_seconds}=    Set Variable    5
${LOGIN-PAGE-FIELD}         android:id/text1
${LOGIN-EMAIL-FIELD}        id=chat21.android.demo:id/email
${LOGIN-PASSWORD-FIELD}     id=chat21.android.demo:id/password
${LOGIN-SIGNIN-BUTTON}      id=chat21.android.demo:id/login


#*** Main Page ***
${MAIN-HOME-TAB}        //android.widget.TextView[@text="HOME"]
${MAIN-PROFILE-TAB}        //android.widget.TextView[@text="PROFILE"]
${MAIN-CHAT-TAB}     //android.widget.TextView[@text="CHAT"]

#*** Profile Page ***
${MAIN-LOGOUT-BUTTON}       id=chat21.android.demo:id/logout


#*** Chat ***
${CHAT-MESSAGE-BUTTON}      id=chat21.android.demo:id/button_new_conversation
${CHAT-LIST-CONTACT}        id=chat21.android.demo:id/contacts_list
${CHAT-SEARCH-CONTACT}      id=chat21.android.demo:id/action_search
${CHAT-SEARCH-TEXT}      id=chat21.android.demo:id/search_src_text
${CHAT-DEF-CONTACT}      //android.widget.TextView[@text="Gabriel Quesada"]
&{CONTACT-01}       contact=Ahabwe


#*** Messages ***
${INPUTMESSAGE-01}           id=chat21.android.demo:id/main_activity_chat_bottom_message_edittext
&{MES-DEF-CLS1}         ms1=Hola


*** Keywords ***
Open Chat21 Application
    [Arguments]     ${APPIUM-PORT}=${APPIUM-PORT-DEVICE1}
    Open Application    http://localhost:${APPIUM-PORT}/wd/hub      platformName=Android     deviceName=emulator-5554     appPackage=chat21.android.demo       appActivity=chat21.android.demo.SplashActivity        automationName=Uiautomator2
#    ${ALERT}        Run Keyword And Return Status       Page Should Not Contain Element ${ANDROID10-CONTINUE-BUTTON}
#    run keyword if  '${ALERT}' == 'False'       Bypass Android 10 Alerts

#Open Chat21 Application On First Device
#    Open Chat21 Application         ${APPIUM-PORT-DEVICE1}

#Open Chat21 Application On Second Device
#   Open Chat21 Application         ${APPIUM-PORT-DEVICE2}


#Bypass Android 10 Alerts
#    Wait Until Page Contains Element        ${ANDROID10-CONTINUE-BUTTON}
#    Click Element                           ${ANDROID10-CONTINUE-BUTTON}
#    Wait Until Page Contains Element        ${ANDROID10-OK-BUTTON}
#    Click Element                           ${ANDROID10-OK-BUTTON}'''

Signing With User
    [Arguments]     ${EMAIL}        ${USERPASSWORD}
    Input User Email        ${EMAIL}
    Input User Password        ${USERPASSWORD}
    Submit Login
    Verify Login Is Successful

Input User Email
     [Arguments]        ${EMAIL}
     Wait Until Page Contains Element        ${LOGIN-PAGE-FIELD}
     Click element       ${LOGIN-PAGE-FIELD}

Input User Password
    [Arguments]        ${EMAIL}
    Input Text      ${LOGIN-PASSWORD-FIELD}     ${USER2-DETAILS}[password]

Submit Login
    Click element   ${LOGIN-SIGNIN-BUTTON}

Verify Login Is Successful
    Wait Until Page Contains Element       ${MAIN-HOME-TAB}

Logout With User
    Click element   ${MAIN-PROFILE-TAB}
    Wait Until Page Contains Element       ${MAIN-LOGOUT-BUTTON}
    Click element      ${MAIN-LOGOUT-BUTTON}
    Wait Until Page Contains Element        ${LOGIN-PAGE-FIELD}


Go to Profie Page
    CLear Text      ${LOGIN-EMAIL-FIELD}
    Input Text      ${LOGIN-EMAIL-FIELD}        ${USER1-DETAILS}[email]
    Input Text      ${LOGIN-PASSWORD-FIELD}     ${USER1-DETAILS}[password]
    Click element   ${LOGIN-SIGNIN-BUTTON}
    Wait Until Page Contains Element       ${MAIN-HOME-TAB}
    Click element   ${MAIN-PROFILE-TAB}


Click the logout button
    Wait Until Page Contains Element       ${MAIN-LOGOUT-BUTTON}
    Click element      ${MAIN-LOGOUT-BUTTON}

Verify Login Email Field Is Displayed
    Wait Until Page Contains Element        ${LOGIN-PAGE-FIELD}


Go to Chat
    [Arguments]     ${CONTACT-01}
    Wait Until Page Contains Element        ${MAIN-CHAT-TAB}
    Click element       ${MAIN-CHAT-TAB}
    Wait Until Page Contains Element        ${CHAT-MESSAGE-BUTTON}
    click element       ${CHAT-MESSAGE-BUTTON}
    Wait Until Page Contains Element        ${CHAT-LIST-CONTACT}
    #click element       ${CHAT-SEARCH-CONTACT}
    #Input Text      ${CHAT-SEARCH-TEXT}        ${CONTACT-01}[contact]
    Wait Until Page Contains Element        ${CHAT-DEF-CONTACT}
    click element       id=chat21.android.demo:id/profile_picture
    #Close Application

Send Message Randoms
    [Arguments]     ${MES-DEF-CLS1}
    Wait Until Page Contains Element        id=chat21.android.demo:id/main_activity_chat_bottom_message_edittext
    click element       ${INPUTMESSAGE-01}
    Input Text      ${INPUTMESSAGE-01}        ${MES-DEF-CLS1}[ms1]
    Wait Until Page Contains Element        id=chat21.android.demo:id/main_activity_send
    click element       id=chat21.android.demo:id/main_activity_send