*** Settings ***
Library     OperatingSystem

*** Keywords ***
Log My Username
    [Arguments]     ${USERNAME}
    Log             ${USERNAME}

Log My Password
    [Arguments]     ${Password}
    Log             ${Password}

Log My Specific Username And Password
    [Arguments]         ${USERNAME2}     ${PASSWORD}
    Log My Username     ${USERNAME2}
    Log My Password     ${PASSWORD}


*** Variables ***
${MY-VARIABLE}      my test variable
${MY-VARIABLE2}      my test variable2

${GOOGLE-SEARCH-FIELD}      //input[@tittle="Searh"]

@{LIST}     test1 test2 test3
${STRING}=                  cat
${NUMBER}=                  ${1}

&{DICTIONARY}   username=user1   password=demo1
&{DICTIONARY2}   username=user2   password=demo2
